
1
00:00:00,000 --> 00:00:03,074
So far, our syntactic vocabulary was
really minimal.

2
00:00:03,074 --> 00:00:08,059
Essentially, all we had were functions and
functional applications.

3
00:00:08,059 --> 00:00:13,073
In this week, we are going to introduce
two more syntax constructs, namely

4
00:00:13,073 --> 00:00:19,007
conditionals, and value definitions.
In every language, you need a way to

5
00:00:19,007 --> 00:00:24,022
choose between two alternatives.
And for that, Scala has the conditional

6
00:00:24,022 --> 00:00:28,038
expression if else.
It looks like an if else in Java, but it

7
00:00:28,038 --> 00:00:33,074
is used for expressions, not statements.
You see an example here, we define an

8
00:00:33,074 --> 00:00:39,046
absolute function which takes an x which
is an int, and says well if x greater or

9
00:00:39,046 --> 00:00:44,050
equal to zero, x otherwise, minus x.
What you see here is that the if else is

10
00:00:44,050 --> 00:00:48,086
actually an expression.
It's not a statement where you have to set

11
00:00:48,086 --> 00:00:52,063
a variable and then return a variable,
It's an expression.

12
00:00:52,063 --> 00:00:55,826
The x is greater that zero here is an
expression of type Boolean, and such

13
00:00:55,826 --> 00:01:02,099
expressions we sometimes call predicates.
So, Boolean expressions are formed in,

14
00:01:02,099 --> 00:01:09,059
essentially, in the same way as in Java.
So you would, could write true or false as

15
00:01:09,059 --> 00:01:16,026
Boolean constants not b bang, it's written
bang b for negation, conjunction b and b,

16
00:01:16,026 --> 00:01:19,093
another b would be written b and
ampersand, ampersand b.

17
00:01:19,093 --> 00:01:25,091
This junction is written with a double bar
and you would have the usual comparison

18
00:01:25,091 --> 00:01:30,008
operators from equality, inequality to all
the comparisons.

19
00:01:30,008 --> 00:01:34,746
Generally, if an expression is an legal
expression in Java, then you can expect it

20
00:01:34,746 --> 00:01:40,621
to be a legal expression in Scala as well.
Okay.

21
00:01:40,621 --> 00:01:45,458
So, we have seen now the syntax, the
formation rules of Boolean expressions.

22
00:01:45,458 --> 00:01:49,860
But, what about their meaning?
And, we have come to define meaning by the

23
00:01:49,860 --> 00:01:54,054
substitution model and we are going to do
the same thing for Booleans.

24
00:01:54,054 --> 00:01:57,075
So, how do we define meaning for Boolean
expressions?

25
00:01:57,075 --> 00:02:02,081
Well, simply by giving rewrite rules that
give you here on the left some templates

26
00:02:02,081 --> 00:02:06,076
for Boolean expressions, and on the right
how they would rewrite.

27
00:02:06,076 --> 00:02:12,074
Take the first one, not true would give
you false, not false would give you true.

28
00:02:12,074 --> 00:02:19,024
The third rule is about and conjunction
that says true and some other expression e

29
00:02:19,024 --> 00:02:23,048
doesn't matter what it is, would always be
the same as e.

30
00:02:23,048 --> 00:02:28,002
And false and some other expression e
would always be false.

31
00:02:28,002 --> 00:02:31,090
The rules for or are analogous to the
rules for and.

32
00:02:31,090 --> 00:02:37,371
They have the duals of those so true or e
would then always be true of course,

33
00:02:37,371 --> 00:02:41,044
whereas false or e for any expression e
would be e.

34
00:02:41,044 --> 00:02:47,046
What's interesting about that is that and,
and or don't always need their right

35
00:02:47,046 --> 00:02:50,066
operand, that's the e here, to be
evaluated.

36
00:02:50,066 --> 00:02:55,180
Whenever you write e in, in a sentence,
you can just pass the arbitrary

37
00:02:55,180 --> 00:02:59,069
expression.
And here for instance, with a false and e,

38
00:02:59,069 --> 00:03:04,009
we're using two false.
You would have that always without even

39
00:03:04,009 --> 00:03:08,486
looking inside the expression e.
So, e doesn't need to be a value, it

40
00:03:08,486 --> 00:03:14,004
doesn't need to be reduced to a value.
And we say that these expressions use

41
00:03:14,004 --> 00:03:18,055
short circuit evaluation.
Of course, in expressions in most other

42
00:03:18,055 --> 00:03:22,033
programming languages Java included do the
same thing.

43
00:03:22,033 --> 00:03:27,451
But now, we have a concise model that
shows essentially a rule for when this

44
00:03:27,451 --> 00:03:29,369
happens.
Let's do an exercise.

45
00:03:29,369 --> 00:03:33,845
You have seen re-write rules for Booleans.
What about if then else?

46
00:03:33,845 --> 00:03:40,010
If then else is an expression so we should
have re-write rules that clarify what it

47
00:03:40,010 --> 00:03:44,257
means as well.
Can you give me two rewrite rules for if

48
00:03:44,257 --> 00:03:51,135
then else?
So, let's see how we would go about that.

49
00:03:51,135 --> 00:03:58,005
So, if else, let's write a form.
So, the general form would be, well here,

50
00:03:58,005 --> 00:04:02,600
we would have a Boolean expression.
Then, we would have a then part, and then

51
00:04:02,600 --> 00:04:06,068
we would have an else part.
Call that e2 and e1, let's say.

52
00:04:06,068 --> 00:04:10,048
So, we need to rewrite rules for this form
of expression.

53
00:04:10,048 --> 00:04:15,050
And, of course, it will depend on whether
this condition b is true or false.

54
00:04:15,050 --> 00:04:20,835
So, let's try first with true.
So, if you have something like, if true,

55
00:04:20,835 --> 00:04:26,086
e1 else e2.
Then, what we would know is that, that

56
00:04:26,086 --> 00:04:31,023
evaluates to the then part.
So, that's it.

57
00:04:31,023 --> 00:04:36,099
It would rewrite to e1.
Whereas, if we have an expression if

58
00:04:36,099 --> 00:04:47,645
false, e1 else e2 then, obviously that
would take the else path and it would

59
00:04:47,645 --> 00:04:51,345
reevaluate to e2.
So, with those two rules we have

60
00:04:51,345 --> 00:04:56,096
characterized the behavior of if else
completely and accurately.

61
00:04:56,096 --> 00:05:01,973
Here's another piece of news and text.
We have seen that function parameters can

62
00:05:01,973 --> 00:05:07,049
be passed by value or by name.
And, in fact, the same distinction applies

63
00:05:07,049 --> 00:05:11,411
to definition.
The def form is, in a sense, call by name

64
00:05:11,411 --> 00:05:15,534
because its right hand side is evaluated
on each use.

65
00:05:15,534 --> 00:05:21,487
There's also a val form which is by value.
For instance, if you write val x equals

66
00:05:21,487 --> 00:05:26,371
two, and then val y equals square of x,
then the square of x will be evaluated

67
00:05:26,371 --> 00:05:30,030
right here instead of when we first
referred to the name y.

68
00:05:30,030 --> 00:05:34,087
So, the right hand side of a value
definition is evaluated at the point of

69
00:05:34,087 --> 00:05:38,058
the definition itself.
And afterwards, the name refers to the

70
00:05:38,058 --> 00:05:41,520
value.
So, in our case, the y reference here

71
00:05:41,520 --> 00:05:46,080
would refer to the number four, not to the
expression square of two.

72
00:05:46,080 --> 00:05:53,005
The difference between val and def as a
definition form becomes apparent when the

73
00:05:53,005 --> 00:06:00,643
right hand side does not terminate.
So, to do that, I can take the repro.

74
00:06:00,643 --> 00:06:09,384
Let's generate another looping expression
again.

75
00:06:09,384 --> 00:06:15,650
There we go.
And now if we say def x equals loop, then

76
00:06:15,650 --> 00:06:20,067
nothing happens.
We just defined another name for loop.

77
00:06:20,067 --> 00:06:28,034
Whereas, if I define val x equals loop,
then my repo dies and doesn't, I have to

78
00:06:28,034 --> 00:06:36,039
take it out explicitly with a Control+C.
So, def x equals loop is okay, but val x

79
00:06:36,039 --> 00:06:42,081
equals loop will lead to an infinite loop
because the right hand side loop will be

80
00:06:42,081 --> 00:06:47,030
evaluated at the point of definition.
Let's do an exercise.

81
00:06:47,030 --> 00:06:53,042
What I want you to do is write functions
and, and or, such that for all argument

82
00:06:53,042 --> 00:06:58,767
expressions x and y, and x, y is the same
as the double ampersand, and or x, y is

83
00:06:58,767 --> 00:07:04,040
the same as the double bar.
And, of course, you shouldn't use bar or

84
00:07:04,040 --> 00:07:10,018
ampersand in your implementations.
When you think about it, it's also good to

85
00:07:10,018 --> 00:07:14,030
think what are good operands to test that
these equalities hold.

86
00:07:15,039 --> 00:07:18,972
Okay.
Let's think about how we would write the

87
00:07:18,972 --> 00:07:24,164
and function.
Let me first give it signature here, so it

88
00:07:24,164 --> 00:07:28,039
takes two Booleans.
And what should it return?

89
00:07:28,039 --> 00:07:34,339
Well, one guideline would be let's just
look at the re-write rules for and.

90
00:07:34,339 --> 00:07:41,582
Let's, as well, if the first condition is
true, then it would re-write to the second

91
00:07:41,582 --> 00:07:45,959
one y.
If the first condition is false then the

92
00:07:45,959 --> 00:07:52,041
result would be false.
And true false is false as expected and

93
00:07:52,041 --> 00:07:55,630
true, true.
So, here we would expect true.

94
00:07:55,630 --> 00:08:01,595
It's true also as expected.
Is true and false the only value we should

95
00:08:01,595 --> 00:08:04,523
test the and with?
Well, maybe not.

96
00:08:04,523 --> 00:08:09,628
What about non-terminating arguments?
Let's try that.

97
00:08:09,628 --> 00:08:16,032
And false and loop.
What would we, would we expect here?

98
00:08:16,032 --> 00:08:23,063
Our rewrite rules says false and loop be
rewritten to false so that's what we would

99
00:08:23,063 --> 00:08:26,147
expect here.
But, what we get is nothing.

100
00:08:26,147 --> 00:08:33,031
So, we get a, an, an infinite loop.
We have to interrupt the execution by a

101
00:08:33,031 --> 00:08:36,637
signal.
So, obviously, there's something wrong

102
00:08:36,637 --> 00:08:39,017
here.
How do we correct it?

103
00:08:39,017 --> 00:08:45,324
Well, if you look at it, the thing that
went wrong here is that we passed the

104
00:08:45,324 --> 00:08:51,539
second argument y as a valued parameter.
If we do that, then, it means that it will

105
00:08:51,539 --> 00:08:57,424
always be evaluated whereas what we need
is to be able to pass it so simply as an

106
00:08:57,424 --> 00:09:01,108
expression.
So, let's try to change that.

107
00:09:01,108 --> 00:09:09,920
We make here the second perimeter a call
by name perimeter, and give it the same

108
00:09:09,920 --> 00:09:15,581
implementation.
And now, we can redo our expression and

109
00:09:15,581 --> 00:09:21,056
false loop, and get false as expected.
